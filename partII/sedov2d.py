import numpy as np
import matplotlib.pyplot as plt
import osiris as pp

for nout in range(1,31):
  # Create figure canvas with space for two plots
  fig = plt.figure()
  ratio = 0.35
  sizex = 20.0
  fig.set_size_inches(sizex,ratio*sizex)
  ax1 = fig.add_subplot(121)
  ax2 = fig.add_subplot(122)

  mydata = pp.RamsesData(nout=nout,verbose=False,center=[0.6,0.0,0])

  # find index, i, of cell with the highest density and print corresponding radius
  # Shows how to access the one dimensional array containing the data in the cells
  i = np.argmax(mydata.data["density"]["values"])
  maxd = np.max(mydata.data["density"]["values"])
  print "Time="+str(mydata.info["time"])+ \
        " Max density="+str(maxd)+ \
        " at radius="+str(mydata.data["r"]["values"][i])

  # make a plot with color indicating density, and arrow size / direciton velocity field in pane nr 1 (ax1)
  mydata.plot_slice(var="log_rho",vec="velocity",vskip=4,axes=ax1)
  
  # make a plot in pane 2 (ax2) with density as a function of radius for all cells
  mydata.plot_histogram("r","log_rho",axes=ax2,marker="s",scatter=True,markeredgecolor='k',color='None',markersize=3)
  
  # save figure to pdf
  fname = "sedov_{:02d}.pdf".format(nout)
  fig.savefig(fname,bbox_inches="tight")

  plt.close(fig)
